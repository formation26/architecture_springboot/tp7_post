package fr.epita.tp7.exposition;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.epita.tp7.domaine.User;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@PostMapping("/create")
	public void create(@RequestBody User u) {
		
		System.out.println("nom "+u.getNom());
		System.out.println("prenom "+u.getPrenom());
	}

}
